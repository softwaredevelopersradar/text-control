﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TextControl;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //textControl.Clear();
            //textControl2.Clear();
            //textControl3.Clear();
            //textControl4.Clear();
            //textControl5.Clear();
            textControl6.Clear();
            textControl7.Clear();
        }

        private void Button_Click1(object sender, RoutedEventArgs e)
        {
            //textControl.Frequency = 325245.5;
            //textControl2.Frequency = 325245.5;
            //textControl3.Frequency = 325245.5;
            //textControl4.Frequency = 325245.5;
            //textControl5.Frequency = 325245.5;
            textControl6.Frequency = 325245.5;
            textControl7.Frequency = 325245.5;
            textControl7.Frequency2 = 325245.5;
            textControl7.Bearing = 345.5;
            textControl7.Bearing2 = 247.5;
        }

        private void Button_Click2(object sender, RoutedEventArgs e)
        {
            var a = textControl.AvBearings;
            var b = textControl.AvPhases;
        }

        private void Button_Click3(object sender, RoutedEventArgs e)
        {
            textControl.Frequency = 5999.5;
        }

        private void Button_Click4(object sender, RoutedEventArgs e)
        {
            textControl7.SetLanguage("rus");
        }

        private void Button_Click5(object sender, RoutedEventArgs e)
        {
            textControl7.SetLanguage("eng");
        }
    }
}
