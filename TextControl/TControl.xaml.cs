﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Xceed.Wpf.Toolkit;

namespace TextControl
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class TControl : UserControl
    {
        public TControl()
        {
            InitializeComponent();
            TopPanelVisibile(_TopPanelVisibility);
        }

        private bool _TopPanelVisibility = false;
        public bool TopPanelVisibility
        {
            get
            {
               return _TopPanelVisibility;
        }
            set
            {
                _TopPanelVisibility = value;
                TopPanelVisibile(_TopPanelVisibility);
            }
        }

        private void TopPanelVisibile(bool flag)
        {
            myGrid.RowDefinitions[0].Height = new GridLength((flag) ? 27 : 0);
        }


        public delegate void SimpleIntEventHandler(int value);
        public event SimpleIntEventHandler NofAveragePhases;
        public event SimpleIntEventHandler NofAverageBearings;



        #region Properties

        private string _sFreq;
        public string sFrequency
        {
            get { return _sFreq; }
            set
            {
                _sFreq = value;
                tFreq.Text = _sFreq;
            }
        }

        private string _sBear;
        public string sBearing
        {
            get { return _sBear; }
            set
            {
                _sBear = value;
                tBear.Text = _sBear;
            }
        }

        private string _sSD;
        public string sSD
        {
            get { return _sSD; }
            set
            {
                _sSD = value;
                tSD.Text = _sSD;
            }
        }

        private string _sLeavePercent;
        public string sLeavePercent
        {
            get { return _sLeavePercent; }
            set
            {
                _sLeavePercent = value;
                tPerc.Text = _sLeavePercent;
            }
        }

        private string _sAvPhases;
        public string sAvPhases
        {
            get { return _sAvPhases; }
            set
            {
                _sAvPhases = value;
                tAPh.Text = _sAvPhases;
            }
        }

        private string _sAvBearings;
        public string sAvBearings
        {
            get { return _sAvBearings; }
            set
            {
                _sAvBearings = value;
                tAPl.Text = _sAvBearings;
            }
        }

        private double _Freq;
        public double Frequency
        {
            get { return _Freq; }
            set
            {
                _Freq = value;
                tbFreq.Text = _Freq.ToString("F4");
            }
        }

        private double _Bear;
        public double Bearing
        {
            get { return _Bear; }
            set
            {
                _Bear = value;
                tbBear.Text = _Bear.ToString("F1");
            }
        }

        private double _SD;
        public double SD
        {
            get { return _SD; }
            set
            {
                _SD = value;
                tbSD.Text = _SD.ToString("F1");
            }
        }

        private double _LeavePercent;
        public double LeavePercent
        {
            get { return _LeavePercent; }
            set
            {
                _LeavePercent = value;
                tbPerc.Text = _LeavePercent.ToString("F1");
            }
        }

        private int _AvPhases = 3;
        public int AvPhases
        {
            get { return _AvPhases; }
            set
            {
                _AvPhases = value;
                iPh.Value = _AvPhases;
            }
        }

        private int _AvBearings = 3;
        public int AvBearings
        {
            get { return _AvBearings; }
            set
            {
                _AvBearings = value;
                iPl.Value = _AvBearings;
            }
        }

        #endregion

        public void Clear()
        {
            for (int i = 0; i < rPanel.Children.Count; i++)
            {
                var children = rPanel.Children[i];
                if (children is TextBox)
                {
                    var t = (TextBox)children;
                    t.Text = "";
                }
                if (children is IntegerUpDown)
                {
                    var t = (IntegerUpDown)children;
                    t.Text = "";
                }
            }
        }


        private void iPh_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (iPh?.Value > iPh?.Maximum || iPh?.Value < iPh?.Minimum)
            {
                iPh.Value = 3;
            }
            if (iPh?.Value * iPl?.Value > 500)
            {
                int value = 500 / (int)iPh.Value;
                iPl.Value = value;
                NofAverageBearings?.Invoke(iPl.Value.Value);
            }
            AvPhases = iPh.Value.Value;
            NofAveragePhases?.Invoke(iPh.Value.Value);
        }

        private void iPl_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (iPl?.Value > iPl?.Maximum || iPl?.Value < iPl?.Minimum)
            {
                iPl.Value = 3;
            }
            if (iPh?.Value * iPl?.Value > 500)
            {
                int value = 500 / (int)iPl.Value;
                iPh.Value = value;
                NofAveragePhases?.Invoke(iPh.Value.Value);
            }
            AvBearings = iPl.Value.Value;
            NofAverageBearings?.Invoke(iPl.Value.Value);
        }

        private void bAuto_Click(object sender, RoutedEventArgs e)
        {

        }

        private void bClear_Click(object sender, RoutedEventArgs e)
        {

        }

        private void bW_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
