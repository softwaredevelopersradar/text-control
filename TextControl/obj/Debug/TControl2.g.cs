﻿#pragma checksum "..\..\TControl2.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "B122F1C1FF6BE02E86E330DBA7BFB8B0351D17D9"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using TextControl;
using Xceed.Wpf.Toolkit;
using Xceed.Wpf.Toolkit.Chromes;
using Xceed.Wpf.Toolkit.Core.Converters;
using Xceed.Wpf.Toolkit.Core.Input;
using Xceed.Wpf.Toolkit.Core.Media;
using Xceed.Wpf.Toolkit.Core.Utilities;
using Xceed.Wpf.Toolkit.Panels;
using Xceed.Wpf.Toolkit.Primitives;
using Xceed.Wpf.Toolkit.PropertyGrid;
using Xceed.Wpf.Toolkit.PropertyGrid.Attributes;
using Xceed.Wpf.Toolkit.PropertyGrid.Commands;
using Xceed.Wpf.Toolkit.PropertyGrid.Converters;
using Xceed.Wpf.Toolkit.PropertyGrid.Editors;
using Xceed.Wpf.Toolkit.Zoombox;


namespace TextControl {
    
    
    /// <summary>
    /// TControl2
    /// </summary>
    public partial class TControl2 : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 20 "..\..\TControl2.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid grid;
        
        #line default
        #line hidden
        
        
        #line 22 "..\..\TControl2.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid myGrid;
        
        #line default
        #line hidden
        
        
        #line 33 "..\..\TControl2.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel lPanel;
        
        #line default
        #line hidden
        
        
        #line 34 "..\..\TControl2.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock tFreq;
        
        #line default
        #line hidden
        
        
        #line 35 "..\..\TControl2.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock tSD;
        
        #line default
        #line hidden
        
        
        #line 36 "..\..\TControl2.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock tAPh;
        
        #line default
        #line hidden
        
        
        #line 39 "..\..\TControl2.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel rPanel;
        
        #line default
        #line hidden
        
        
        #line 40 "..\..\TControl2.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbFreq;
        
        #line default
        #line hidden
        
        
        #line 41 "..\..\TControl2.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbSD;
        
        #line default
        #line hidden
        
        
        #line 42 "..\..\TControl2.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Xceed.Wpf.Toolkit.IntegerUpDown iPh;
        
        #line default
        #line hidden
        
        
        #line 53 "..\..\TControl2.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel lPanel2;
        
        #line default
        #line hidden
        
        
        #line 54 "..\..\TControl2.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock tBear;
        
        #line default
        #line hidden
        
        
        #line 55 "..\..\TControl2.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock tPerc;
        
        #line default
        #line hidden
        
        
        #line 56 "..\..\TControl2.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock tAPl;
        
        #line default
        #line hidden
        
        
        #line 59 "..\..\TControl2.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel rPanel2;
        
        #line default
        #line hidden
        
        
        #line 60 "..\..\TControl2.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbBear;
        
        #line default
        #line hidden
        
        
        #line 61 "..\..\TControl2.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbPerc;
        
        #line default
        #line hidden
        
        
        #line 62 "..\..\TControl2.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Xceed.Wpf.Toolkit.IntegerUpDown iPl;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/TextControl;component/tcontrol2.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\TControl2.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.grid = ((System.Windows.Controls.Grid)(target));
            return;
            case 2:
            this.myGrid = ((System.Windows.Controls.Grid)(target));
            return;
            case 3:
            this.lPanel = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 4:
            this.tFreq = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 5:
            this.tSD = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 6:
            this.tAPh = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 7:
            this.rPanel = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 8:
            this.tbFreq = ((System.Windows.Controls.TextBox)(target));
            return;
            case 9:
            this.tbSD = ((System.Windows.Controls.TextBox)(target));
            return;
            case 10:
            this.iPh = ((Xceed.Wpf.Toolkit.IntegerUpDown)(target));
            
            #line 42 "..\..\TControl2.xaml"
            this.iPh.ValueChanged += new System.Windows.RoutedPropertyChangedEventHandler<object>(this.iPh_ValueChanged);
            
            #line default
            #line hidden
            return;
            case 11:
            this.lPanel2 = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 12:
            this.tBear = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 13:
            this.tPerc = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 14:
            this.tAPl = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 15:
            this.rPanel2 = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 16:
            this.tbBear = ((System.Windows.Controls.TextBox)(target));
            return;
            case 17:
            this.tbPerc = ((System.Windows.Controls.TextBox)(target));
            return;
            case 18:
            this.iPl = ((Xceed.Wpf.Toolkit.IntegerUpDown)(target));
            
            #line 62 "..\..\TControl2.xaml"
            this.iPl.ValueChanged += new System.Windows.RoutedPropertyChangedEventHandler<object>(this.iPl_ValueChanged);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

