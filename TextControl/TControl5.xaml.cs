﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TextControl
{
    /// <summary>
    /// Interaction logic for TControl5.xaml
    /// </summary>
    public partial class TControl5 : UserControl
    {
        public TControl5()
        {
            InitializeComponent();
        }

        public delegate void SimpleIntEventHandler(int value);
        public event SimpleIntEventHandler NofAveragePhases;
        public event SimpleIntEventHandler NofAverageBearings;

        #region Properties

        private string _sFreq;
        public string sFrequency
        {
            get { return _sFreq; }
            set
            {
                _sFreq = value;
                tFreq.Text = _sFreq;
            }
        }

        private string _sBear;
        public string sBearing
        {
            get { return _sBear; }
            set
            {
                _sBear = value;
                tBear.Text = _sBear;
            }
        }

        private string _sSD;
        public string sSD
        {
            get { return _sSD; }
            set
            {
                _sSD = value;
                tSD.Text = _sSD;
            }
        }

        private string _sLeavePercent;
        public string sLeavePercent
        {
            get { return _sLeavePercent; }
            set
            {
                _sLeavePercent = value;
                tPerc.Text = _sLeavePercent;
            }
        }

        private double _Freq;
        public double Frequency
        {
            get { return _Freq; }
            set
            {
                _Freq = value;
                tbFreq.Text = _Freq.ToString("F4");
            }
        }

        private double _Bear;
        public double Bearing
        {
            get { return _Bear; }
            set
            {
                _Bear = value;
                tbBear.Text = _Bear.ToString("F1");
            }
        }

        private double _SD;
        public double SD
        {
            get { return _SD; }
            set
            {
                _SD = value;
                tbSD.Text = _SD.ToString("F1");
            }
        }

        private double _LeavePercent;
        public double LeavePercent
        {
            get { return _LeavePercent; }
            set
            {
                _LeavePercent = value;
                tbPerc.Text = _LeavePercent.ToString("F1");
            }
        }

        private int _AvPhases = 3;
        public int AvPhases
        {
            get { return _AvPhases; }
            set
            {
                _AvPhases = value;
            }
        }

        private int _AvBearings = 3;
        public int AvBearings
        {
            get { return _AvBearings; }
            set
            {
                _AvBearings = value;
            }
        }

        #endregion

        public void Clear()
        {
            for (int i = 0; i < rPanel.Children.Count; i++)
            {
                var children = rPanel.Children[i];
                if (children is TextBox)
                {
                    var t = (TextBox)children;
                    t.Text = "";
                }
              
            }
        }
    }
}
